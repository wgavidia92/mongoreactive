#MongoReactive

[[_TOC_]]

### 1. Overview

This project follows the guide *[Spring WebFlux Reactive CRUD REST API Example](https://www.javaguides.net/2023/01/spring-webflux-reactive-crud-rest-api.html)* from **[javaguides.net](https://www.javaguides.net/)** to create a POC that works with Spring reactive and a Mongo DB.

### 2. Requirements

To run this project in your local you will need:

* Java 20+
* MongoDB account - Follow the guide [Sign Up for a MongoDB Account](https://www.mongodb.com/docs/guides/atlas/account/)
* MongoDB cluster - Follow the guide [Create a Cluster](https://www.mongodb.com/docs/guides/atlas/cluster/)
* MongoDB user and password - Follow the guide [Add a Database User](https://www.mongodb.com/docs/guides/atlas/db-user/)

### 3. Run the application

To run the application you should follow this steps:

**1** Clone or download this project

**2** Open the project in your favorite IDE, (Eclipse, IntelliJ, etc)

**3** Update the **application.yml** file in the path *MongoReactive/src/main/resources* changing the property `spring.data.mongodb.uri` with yours.


>
> ```properties
> spring:
>   data:
>     mongodb:
>       uri: mongodb+srv://[user]:[password]@[cluster].rlnteys.mongodb.net/?retryWrites=true&w=majority
> ```
>

**4** Run with the IDE or the command `mvn spring-boot:run` from console

### 4. Architecture

1. **controller:** package with the class `EmployeeController.java` where the endpoints are defined.
2. **domain:** package where all the business logic is placed.
	1. **dto:** package with the record `EmployeeDto.java` which is used to transfer the data between the controller and the repository.
	2. **entity:** package with the class `Employee.java` that maps the document from the database.
	3. **Repository:** package with the interface `EmployeeRepository.java` that extends from Spring JPA to manage the persistence.
	4. **Service:** Classes and interfaces to keep the business logic of the application.

![Components](./components.jpg)
