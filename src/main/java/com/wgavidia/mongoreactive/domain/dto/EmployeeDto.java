package com.wgavidia.mongoreactive.domain.dto;

public record EmployeeDto (String id, String firstName, String lastName, String email) {
}
