package com.wgavidia.mongoreactive.domain.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.wgavidia.mongoreactive.domain.entity.Employee;

public interface EmployeeRepository extends ReactiveCrudRepository<Employee, String> {

}
