package com.wgavidia.mongoreactive.domain.service;

import org.springframework.stereotype.Service;

import com.wgavidia.mongoreactive.domain.dto.EmployeeDto;
import com.wgavidia.mongoreactive.domain.entity.Employee;
import com.wgavidia.mongoreactive.domain.repository.EmployeeRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository repository;

	public EmployeeServiceImpl(EmployeeRepository repository) {
		this.repository = repository;
	}

	@Override
	public Mono<EmployeeDto> saveEmployee(EmployeeDto employeeDto) {
		var entity = EmployeeMapper.toEntity(employeeDto);
		return repository.save(entity)
				.map(EmployeeMapper::toDto);
	}

	@Override
	public Mono<EmployeeDto> getEmployee(String employeeId) {
		return repository.findById(employeeId)
			.map(EmployeeMapper::toDto);
	}

	@Override
	public Flux<EmployeeDto> getAllEmployees() {
		return repository.findAll()
			.map(EmployeeMapper::toDto)
			.switchIfEmpty(Flux.empty());
	}

	@Override
	public Mono<EmployeeDto> updateEmployee(EmployeeDto employeeDto, String employeeId) {
		return repository.findById(employeeId)
				.flatMap(entity -> this.updateEmployee(entity, employeeDto))
				.map(EmployeeMapper::toDto);
	}

	private Mono<Employee> updateEmployee(Employee entity, EmployeeDto employeeDto) {
		entity.setFirstName(employeeDto.firstName());
		entity.setLastName(employeeDto.lastName());
		entity.setEmail(employeeDto.email());
		return repository.save(entity);
	}

	@Override
	public Mono<Void> deleteEmployee(String employeeId) {
		return repository.deleteById(employeeId);
	}

}
