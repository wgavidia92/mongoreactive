package com.wgavidia.mongoreactive.domain.service;

import com.wgavidia.mongoreactive.domain.dto.EmployeeDto;
import com.wgavidia.mongoreactive.domain.entity.Employee;

final class EmployeeMapper {

	private EmployeeMapper() {}

	public static Employee toEntity(EmployeeDto dto) {
		return new Employee(
				dto.id(),
				dto.firstName(),
				dto.lastName(),
				dto.email());
	}

	public static EmployeeDto toDto(Employee entity) {
		return new EmployeeDto(
				entity.getId(),
				entity.getFirstName(),
				entity.getLastName(),
				entity.getEmail());
	}

}
