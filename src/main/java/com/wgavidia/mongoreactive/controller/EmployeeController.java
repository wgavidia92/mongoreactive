package com.wgavidia.mongoreactive.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;

import com.wgavidia.mongoreactive.domain.dto.EmployeeDto;
import com.wgavidia.mongoreactive.domain.service.EmployeeService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/employees")
public class EmployeeController {

	private final EmployeeService service;

	public EmployeeController(EmployeeService service) {
		this.service = service;
	}

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public Mono<EmployeeDto> saveEmployee(@RequestBody EmployeeDto employee) {
		return service.saveEmployee(employee);
	}

	@GetMapping("/{id}")
	public Mono<EmployeeDto> getEmployee(@PathVariable("id") String employee) {
		return service.getEmployee(employee);
	}

	@GetMapping
	public Flux<EmployeeDto> getEmployees() {
		return service.getAllEmployees();
	}

	@PutMapping("/{id}")
	public Mono<EmployeeDto> updateEmployee(@RequestBody EmployeeDto employee, @PathVariable("id") String employeeId) {
		return service.updateEmployee(employee, employeeId);
	}

	@DeleteMapping("/{id}")
	public Mono<Void> deleteEmployee(@PathVariable("id") String employee) {
		return service.deleteEmployee(employee);
	}

}
