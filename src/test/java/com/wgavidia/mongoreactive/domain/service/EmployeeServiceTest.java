package com.wgavidia.mongoreactive.domain.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.wgavidia.mongoreactive.domain.repository.EmployeeRepository;

import reactor.core.publisher.Mono;

import com.wgavidia.mongoreactive.domain.dto.EmployeeDto;
import com.wgavidia.mongoreactive.domain.entity.Employee;

class EmployeeServiceTest {

	private EmployeeService service;

	@Mock
	private EmployeeRepository repository;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		service = new EmployeeServiceImpl(repository);
	}

	@Test
	void saveEmployeeTest() {
		var dto = new EmployeeDto(null, "WILMER", "GAVIDIA", "wgavidia@mail.com");
		var expectedId = "TestID";

		Mockito.doAnswer(invocation -> {
			var entity = invocation.getArgument(0, Employee.class);
			entity.setId(expectedId);
			return Mono.just(entity);
		}).when(repository).save(Mockito.any(Employee.class));

		var mono = service.saveEmployee(dto);
		Assertions.assertNotNull(mono);

		var saved = mono.block();
		Assertions.assertNotNull(saved);
		Assertions.assertEquals(expectedId, saved.id());
	}

	@Test
	void getEmployeeWithNoDataTest() {
		Mockito.doAnswer(invocation -> Mono.just(new Employee()))
			.when(repository).findById(Mockito.anyString());

		var mono = service.getEmployee("TestID");
		Assertions.assertNotNull(mono);

		var employee = mono.block();
		Assertions.assertNotNull(employee);
	}

	@Test
	void getEmployeeTest() {
		var employeeId = "TestID";
		Mockito.doAnswer(invocation -> {
			var id = invocation.getArgument(0, String.class);
			var employee = new Employee(id, "WILMER", "GAVIDIA", "wgavidia@mail.com");
			return Mono.just(employee);
		}).when(repository).findById(Mockito.anyString());

		var mono = service.getEmployee(employeeId);
		Assertions.assertNotNull(mono);

		var employee = mono.block();
		Assertions.assertNotNull(employee);
		Assertions.assertEquals(employeeId, employee.id());
	}

}
